bool led1flag = false;
bool led2flag = false;
bool led3flag = false;

unsigned long led1time, led2time, led3time;
unsigned long led1previoustime, led2previoustime, led3previoustime;
unsigned long interval = 2000;


// defines pins numbers
const int trigPin1 = 9;
const int echoPin1 = 10;
const int trigPin2 = 11;
const int echoPin2 = 12;
const int trigPin3 = 8;
const int echoPin3 = 13;

int led1 = 3;
int led2 = 4;
int led3 = 5;

long duration1, duration2, duration3;
int distance1, distance2, distance3;

void setup() {
  pinMode(trigPin1, OUTPUT); 
  pinMode(echoPin1, INPUT);
  pinMode(trigPin2, OUTPUT);
  pinMode(echoPin2, INPUT);
  pinMode(trigPin3, OUTPUT);
  pinMode(echoPin3, INPUT);

  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  Serial.begin(9600); 
}
void loop() {

  digitalWrite(trigPin1, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin1, HIGH);
  delayMicroseconds(10);   
  digitalWrite(trigPin1, LOW);
  duration1 = pulseIn(echoPin1, HIGH);
  distance1 = duration1 * 0.034 / 2;

  digitalWrite(trigPin2, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin2, HIGH);
  delayMicroseconds(10);  
  digitalWrite(trigPin2, LOW);
  duration2 = pulseIn(echoPin2, HIGH);
  distance2 = duration2 * 0.034 / 2;


  digitalWrite(trigPin3, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin3, HIGH);
  delayMicroseconds(10);    
  digitalWrite(trigPin3, LOW);
  duration3 = pulseIn(echoPin3, HIGH);
  distance3 = duration3 * 0.034 / 2;

  // Prints the distance on the Serial Monitor
  Serial.print("Distance: ");
  Serial.println(distance1);
  Serial.println(distance2);
  Serial.println(distance3);

  if (distance1 < 20 ) {
    digitalWrite(led1, HIGH);
    led1flag = true;
    led1time = millis();
  }
  if ( distance2 < 20 ) {
    digitalWrite(led2, HIGH);
    led2flag = true;
    led2time = millis();
  }
  if ( distance3 < 20 ) {
    digitalWrite(led3, HIGH);
    led3flag = true;
    led3time = millis();
  }
  else {
    
    if ((led1time - led1previoustime > interval) && led1flag == true ) {
      digitalWrite(led1, LOW);
      led1flag = false;
      led1previoustime = millis();
    }
    if ((led2time - led2previoustime > interval) && led2flag == true ) {
      digitalWrite(led2, LOW);
      led2flag = false;
      led2previoustime = millis();
    }
    if ((led3time - led3previoustime > interval) && led3flag == true ) {
      digitalWrite(led3, LOW);
      led3flag = false;
      led3previoustime = millis();
    }

  }


}
